let last = 0;

window.addEventListener('devicemotion', function(e) {
    const vx = e.acceleration.x;
    const vy = e.acceleration.y;
    const vz = e.acceleration.z;

    const scalar = Math.sqrt(vx * vx + vy * vy + vz * vz);

    if (Date.now() - last > 1000) {
        last = Date.now();

        if (scalar > 0.01) {
            window.navigator.vibrate(Math.min(scalar * 1000, 100));
        }
    }
});